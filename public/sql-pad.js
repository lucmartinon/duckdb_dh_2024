class SQL_Pad extends HTMLElement {

    connectedCallback() {
        let template = document.getElementById("sql-pad-template");
        const node = document.importNode(template.content, true);
        this.appendChild(node);
        this.editor_el = this.querySelector('.sql-editor');
        this.editor_el.style.height = this.getAttribute(`editor-lines`) * 25 + 30 + "px";
        this.querySelector('.sql-run-button').addEventListener('click', click_run_btn);
    }

    init_monaco(){
        var monaco_editor = monaco.editor.create(
            this.editor_el, {
                value: this.getAttribute('data-query'),
                language: 'sql',
                theme: 'vs-dark',
                fontSize: "14px",
                lineNumbers: "off",
                roundedSelection: false,
                padding: {
                  top: 20,
                  bottom: 20
                }
            });
        monaco_editor.pad = this;


        // let folks use CMD+Enter
        monaco_editor.addAction({
            id: "cmd_enter",
            label: "cmd_enter",
            keybindings: [monaco.KeyMod.CtrlCmd | monaco.KeyCode.Enter],
            run: () => {runSQL(monaco_editor.pad);},
        });
        this.monaco_editor = monaco_editor;
    }
}

customElements.define('sql-pad', SQL_Pad);

function initEditors(){
    for (const ed of document.getElementsByTagName('sql-pad')){
        ed.init_monaco();
    }

    window.addEventListener('resize', () => {
        for (const e of document.getElementsByClassName('sql-editor')){
            e.monaco_editor.layout();
        }
    });
}

async function click_run_btn(event){
    let pad = event.currentTarget.parentElement.parentElement;
    runSQL(pad);
}

async function runSQL(pad){

    pad.querySelector('.sql-error').innerHTML = ""
    pad.querySelector('.sql-output').innerHTML = ""
    pad.querySelector('.sql-run-status').innerHTML = "Executing query"


    // retrieve the query text from the monaco editor pane
    var sql = pad.monaco_editor.getValue();

    if (sql.toLowerCase().startsWith('select')) {
        sql = 'select * from (' + sql + ') limit 100;'
    }

    document.body.style.cursor = "wait";
    pad.monaco_editor.updateOptions({ readOnly: true })

    // use try…catch so we can show errors
    try { // make a table from the resultset
        const res = await window.db.query(sql)
        const arr = [...res].map(d => d.toJSON());
        let html_table = json_to_table(arr);
        pad.querySelector('.sql-output').appendChild(html_table);
        var height = Math.min(html_table.offsetHeight, 300);
        pad.querySelector('.sql-output').style.height = height  + 15 + 'px';
    } catch (err) {
        pad.querySelector('.sql-error').innerHTML = err.message;
    } finally {
        document.body.style.cursor = "default";
        pad.monaco_editor.updateOptions({ readOnly: false });
        pad.querySelector('.sql-run-status').innerText = "";
    }
}



function json_to_table(arr){

    // Create table.
    const table = document.createElement("table");

    let tr = table.insertRow(-1);

    // Extract value from table header and create first row with th.
    let headers = [];
    for (let key in arr[0]) {
        headers.push(key);
        let th = document.createElement("th");
        th.innerHTML = key;
        tr.appendChild(th);
    }


    // add json data to the table as rows.
    for (let i = 0; i < arr.length; i++) {
      tr = table.insertRow(-1);

      for (let j = 0; j < headers.length; j++) {
        let tabCell = tr.insertCell(-1);
        tabCell.innerHTML = arr[i][headers[j]];
      }
    }
    return table;
}